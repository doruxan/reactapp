import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Backdrop from "@material-ui/core/Backdrop";
import { useSpring, animated } from "react-spring/web.cjs";
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Fab from '@material-ui/core/Fab';
import SendIcon from '@material-ui/icons/Send';
import axios from 'axios'
import {
    JsonHubProtocol,
    HubConnectionState,
    HubConnectionBuilder,
    LogLevel
} from '@aspnet/signalr';

const useStyles = makeStyles(theme => ({
    container: {
        flex: 1
    },
    table: {
        minWidth: 300,
    },
    chatSection: {
        // maxWidth: '80%',
        // height: '80vh'
        flex: 1
    },
    headBG: {
        backgroundColor: '#e0e0e0'
    },
    borderRight500: {
        borderRight: '1px solid #e0e0e0'
    },
    messageArea: {
        height: '70vh',
        overflowY: 'auto'
    },
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: "1px solid #000",
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        borderRadius: '5px',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
    }
}));

const Fade = React.forwardRef(function Fade(props, ref) {
    const { in: open, children, onEnter, onExited, ...other } = props;
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
        onStart: () => {
            if (open && onEnter) {
                onEnter();
            }
        },
        onRest: () => {
            if (!open && onExited) {
                onExited();
            }
        }
    });
    return (
        <animated.div ref={ref} style={style} {...other}>
            {children}
        </animated.div>
    );
});


const Home = () => {
    const classes = useStyles();
    const [username, setUsername] = useState("");
    const [userId, setUserId] = useState("");
    const [usernameInput, setUsernameInput] = useState("");
    const [channels, setChannels] = useState([]);
    const [channel, setChannel] = useState(null);
    const [message, setMessage] = useState("");
    const [messages, setMessages] = useState([]);
    const [typing, setTyping] = useState(false);
    const [addChannelModal, setAddChannelModal] = useState(false);
    const [addChannelModalName, setAddChannelModalname] = useState("");
    const [hubConnection, setHubConnection] = useState(null);
    const [pageRef, setPageRef] = useState("")
    const scrollRef = useRef(null)
    const [latestMessage, setLatestMessage] = useState(null)
    const [latestCorrelation, setLatestCorrelation] = useState(null)
    const [latestChannel, setLatestChannel] = useState(null)

    const uuidv4 = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    const getRooms = async () => {
        axios.get('https://localhost:5001/api/chat/GetRooms')
            .then(res => {
                console.log(res);
                console.log(res.data);
                setChannels(res.data);
            })
            .catch(error => {
                console.log('getRoomsError', error)
            })
    }

    const connectToHub = () => {
        const hubConnection = new HubConnectionBuilder()
            .withUrl("https://localhost:5001/ws")
            .configureLogging(LogLevel.Debug)
            .build()

        hubConnection
            .start()
            .then(() => console.log('Connection started!'))
            .catch(err => console.log('Error while establishing connection', err));

        hubConnection.on('receiveMessage', (received) => {

            setLatestMessage(received)
        });

        hubConnection.on('receiveCorrelation', (received) => {
            setLatestCorrelation(received)
        });

        hubConnection.on('newRoom', (received) => {
            console.log('channelReceived')
            setLatestChannel(received)
        });

        setHubConnection(hubConnection);
    }

    useEffect(() => {

        getRooms();
        connectToHub();


        return () => {

        }
    }, [])

    useEffect(() => {
        if(!latestMessage)return;
        var channelMessages = messages
        var foundIndex = channelMessages.findIndex(x => x.correlationId == message.correlationId);
        if (foundIndex < 0) {
            channelMessages.push(latestMessage)
            setMessages(channelMessages)
            console.log('scrollRef', scrollRef)
            if (scrollRef.current) {
                scrollRef.current.scrollIntoView({ behaviour: "smooth" });
            }
            setMessage("")
            setPageRef(latestMessage.id)
        }
    }, [latestMessage])

    useEffect(() => {

        if(!latestCorrelation)return;
        var channelMessages = messages
        var foundIndex = channelMessages.findIndex(x => x.correlationId == latestCorrelation.correlationId);
        if (foundIndex > -1) {
            var relatedMessage = channelMessages[foundIndex];
            relatedMessage.id = latestCorrelation.id
            channelMessages[foundIndex] = relatedMessage;
            setMessages(channelMessages)
            console.log('scrollRef', scrollRef)
            if (scrollRef.current) {
                scrollRef.current.scrollIntoView({ behaviour: "smooth" });
            }
            setMessage("")
            setPageRef(latestCorrelation.id)
        }

    }, [latestCorrelation])

    useEffect(() => {
        if(!latestChannel)return;
        console.log('here',latestChannel)
        var existingChannels = Object.assign([], channels)
        existingChannels.push(latestChannel)
        console.log('existingChannels',existingChannels)
        setChannels(existingChannels)
    }, [latestChannel])


    const addChannel = () => {
        axios.post('https://localhost:5001/api/chat/createRoom', { roomName: addChannelModalName })
            .then(res => {
                console.log(res);
                console.log(res.data);
                // getRooms()
                setAddChannelModal(false)

            })
            .catch(error => {
                console.log('addChannelERror', error)
                setAddChannelModal(false)
            })
    }

    const joinChannel = (id) => {
        if (!!channel)
            leaveSignalChannel(channel.id)

        axios.get(`https://localhost:5001/api/chat/getRoom?roomId=${id}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
                var room = res.data
                setChannel(room)
                setMessages(room.messages)
                joinSignalChannel(res.data.id)
                console.log('joinedChannel', channel)
            })
            .catch(error => {
                console.log('getChannelERror', error)
                setAddChannelModal(false)
            })

    }


    const sendMessage = () => {
        hubConnection
            .invoke('SendMessageToGroup', { message, userId: userId.toString(), roomId: channel.id.toString(), username, correlationId: uuidv4() })
            .catch(err => console.error(err));

    }

    const joinSignalChannel = (channelId) => {
        try {
            console.log('channelIdJoin', channelId)
            hubConnection
                .invoke('joinGroup', channelId)
                .catch(err => console.error(err));

        } catch (error) {
            console.log('joinSignalChannelError', error)
        }

    }

    const leaveSignalChannel = (channelId) => {
        try {
            console.log('channelIdleave', channelId)
            // setChannel(null)
            // setMessages([])
            hubConnection
                .invoke('leaveGroup', channelId)
                .catch(err => console.error(err));

        } catch (error) {
            console.log('LEAVESignalChannelError', error)
        }
    }

    const submitUser = () => {
        axios.post('https://localhost:5001/api/user/LoginUser', { username: usernameInput })
            .then(res => {
                console.log(res);
                console.log(res.data);
                setUsername(res.data.username)
                setUserId(res.data.id)
            })
            .catch(error => {
                console.log('addUserError', error)
                setAddChannelModal(false)
            })

    }


    return (
        <div className={classes.container}
        // key={pageRef}
        >

            <Grid container component={Paper} className={classes.chatSection}>
                <Grid item xs={3} className={classes.borderRight500}>
                    <List>
                        {!!username ?
                            <ListItem button key={username}>
                                {/* <ListItemIcon>
                                    <Avatar alt={username} src="https://material-ui.com/static/images/avatar/1.jpg" />
                                </ListItemIcon> */}
                                <ListItemText primary={username}></ListItemText>
                                {/* <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setUsername("")}
                                >
                                    Change
                                </Button> */}
                            </ListItem>
                            :
                            <Grid item xs={12} style={{ padding: '10px', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'space-between', display: 'flex' }}>
                                <TextField
                                    id="outlined-basic-email"
                                    label="Username"
                                    variant="outlined"
                                    // fullWidth 
                                    onChange={e => setUsernameInput(e.target.value)}
                                    value={usernameInput}
                                    style={{ width: '70%' }}
                                />
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={() => submitUser(usernameInput)}
                                >
                                    Submit
                                </Button>
                            </Grid>
                        }
                    </List>
                    <Divider />

                    <Divider />
                    {!!userId &&
                        <>
                            <Grid container>
                                <Grid item xs={12} style={{ textAlign: 'center', marginTop: '10px', marginBottom: '10px' }}>
                                    <Typography variant="h6" className="header-message">Channels</Typography>
                                </Grid>
                            </Grid>
                            <Divider />
                            <List>
                                {!!channels && channels.map(listChannel => (
                                    <ListItem button key={listChannel.roomName} onClick={() => joinChannel(listChannel.id)}>
                                        {/* <ListItemIcon>
                                    <Avatar alt={channel} src="https://material-ui.com/static/images/avatar/1.jpg" />
                                </ListItemIcon> */}
                                        <ListItemText primary={listChannel.roomName}>{listChannel.roomName}</ListItemText>
                                        <ListItemText secondary="online" align="right"></ListItemText>
                                    </ListItem>
                                ))}
                                <ListItem key="addButton" style={{ justifyContent: 'center' }} >
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => setAddChannelModal(true)}
                                    >
                                        Add Channel
                                </Button>
                                </ListItem>
                            </List>
                        </>
                    }
                </Grid>
                <Grid item xs={9}>
                    {!!channel ?
                        <>
                            <Grid container>
                                <Grid item xs={12} style={{ textAlign: 'center', marginTop: '10px', marginBottom: '10px' }}>
                                    <Typography variant="h6" className="header-message">{channel.roomName}</Typography>
                                </Grid>
                            </Grid>
                            <Divider />
                            <List className={classes.messageArea}>
                                {!!messages && messages.sort(x => x.unixTimestamp).map(message => (
                                    <ListItem key={message.id}>
                                        <Grid container>
                                            <Grid
                                                item
                                                xs={12}
                                            // style={{backgroundColor:message.userId == userId ? "pink" : "blue", alignSelf:message.userId == userId ? "flex-end" : "flex-start"}}
                                            >
                                                <ListItemText align={message.userId == userId ? "right" : "left"} secondary={message.username}></ListItemText>
                                                <ListItemText align={message.userId == userId ? "right" : "left"} primary={message.message}></ListItemText>
                                            </Grid>
                                            {/* <Grid item xs={12}>
                                            </Grid> */}
                                        </Grid>
                                    </ListItem>
                                ))}
                                <div ref={scrollRef} />
                            </List>
                            <Divider />
                            <Grid container style={{ padding: '20px' }}>
                                <Grid item xs={11}>
                                    <TextField id="outlined-basic-email" value={message} onChange={e => setMessage(e.target.value)} label="Type Something" fullWidth />
                                </Grid>
                                <Grid xs={1} align="right">
                                    <Fab onClick={() => sendMessage()} color="primary" aria-label="add"><SendIcon /></Fab>
                                </Grid>
                            </Grid>
                        </>
                        :
                        <Grid container className={classes.messageArea}>
                            <Grid item xs={12} style={{ textAlign: 'center', marginTop: '10px', marginBottom: '10px', height: '100%', flex: 1 }}>
                                {!!userId ?
                                    <Typography variant="h6" className="header-message">Click a channel to see messages</Typography>
                                    :
                                    <Typography variant="h6" className="header-message">Submit a username to see channels</Typography>
                                }
                            </Grid>
                        </Grid>}
                </Grid>
            </Grid>
            {/* <Modal
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h2>Spring modal</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </Fade>
            </Modal> */}

            <Modal
                className={classes.modal}
                open={addChannelModal}
                onClose={() => setAddChannelModal(false)}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500
                }}
            >
                <Fade in={addChannelModal}>
                    <div className={classes.paper}>
                        <h2>Add Channel</h2>
                        <TextField
                            id="outlined-basic-email"
                            label="Channel Name"
                            variant="outlined"
                            // fullWidth 
                            onChange={e => setAddChannelModalname(e.target.value)}
                            value={addChannelModalName}
                            style={{ width: '100%' }}
                        />
                        <div style={{ marginTop: '10px', justifyContent: 'space-between', display: 'flex', flexDirection: 'row', width: '100%' }}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => addChannel(usernameInput)}
                            >
                                Submit
                         </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => setAddChannelModal(false)}
                            >
                                Close
                         </Button>
                        </div>

                    </div>
                </Fade>
            </Modal>


        </div>
    );
}

export default Home;