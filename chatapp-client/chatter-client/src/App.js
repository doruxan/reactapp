import logo from './logo.svg';
import './App.css';
import Home from './containers/home/home.container'

function App() {
  return (
    <div className="App">

      <Home />
    </div>
  );
}

export default App;
